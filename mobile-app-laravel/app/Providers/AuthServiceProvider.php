<?php

namespace App\Providers;

use Laravel\Passport\Passport;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

/**STEP 5 in this reference: https://blog.flicher.net/laravel-rest-api-passport-authentication-for-ionic-app/ */

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        /**This method will register the routes necessary to issue 
        * access tokens and revoke access tokens, 
        * clients, and personal access tokens.
        */
        Passport::routes();
    }
}
