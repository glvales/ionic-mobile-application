<?php

namespace App;

use Laravel\Passport\HasApiTokens; //added passport trait
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

/**After running the STEP 4 which is generating keys, we need to do some modification in some files. */

/**STEP 5 in this reference: https://blog.flicher.net/laravel-rest-api-passport-authentication-for-ionic-app/ */
class User extends Authenticatable
{
    use Notifiable, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'password', //modified
    ]; //original -> 'name', 'email', 'password',


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
