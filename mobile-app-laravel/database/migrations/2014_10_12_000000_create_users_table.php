<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

     /**As laravel comes with users migrations by default, which is located at database/migrations/create_users_table.php. */
     /**You can also update this according to your requirement. I will just add two new columns instead of name column. */
     /**To be able to migrate the database in case you haven't set up the database, go the .env file */
     /**Now that we have modified the .env with our database name, run this command: php artisan migrate */

     /**STEP 3 in this reference: https://blog.flicher.net/laravel-rest-api-passport-authentication-for-ionic-app/ */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            //$table->string('name');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
