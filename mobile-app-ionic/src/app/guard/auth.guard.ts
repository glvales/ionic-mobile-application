import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';

/** So when our app loads, it will check for auth token. 
 * If token exists, which means user is logged in and 
 * app will let user go to dashboard, Otherwise if token 
 * doesn’t exists, app will take user to landing page 
 * which will prompt user to login or register. 
*/

@Injectable({
  providedIn: 'root'
})

export class AuthGuard implements  CanActivate {
  constructor(
    private router: Router,
    private authService: AuthService
  ) {}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      const currentUser = this.authService.isLoggedIn;
      if (currentUser) {
          // authorised so return true
          return true;
      }
      // not logged in so redirect to login page with the return url
      this.router.navigate(['/landing']);
      return false;
  }

}
