import { Component, OnInit } from '@angular/core';
import { ModalController, NavController } from '@ionic/angular';
import { LoginPage } from '../login/login.page';
import { AuthService } from 'src/app/services/auth.service';
import { NgForm } from '@angular/forms';
import { AlertService } from 'src/app/services/alert.service';
@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  constructor(private modalController: ModalController,
    private authService: AuthService,
    private navCtrl: NavController,
    private alertService: AlertService
  ) { }
  ngOnInit() {
  }
  // Dismiss Register Modal
  dismissRegister() {
    this.modalController.dismiss();
  }
  // On Login button tap, dismiss Register modal and open login Modal
 async loginModal() {
   this.dismissRegister();
   const loginModal = await this.modalController.create({
     component: LoginPage,
  });
   return await loginModal.present();
 }
  register(form: NgForm) {
    this.authService.register(form.value.fName, form.value.lName, form.value.email, form.value.password).subscribe(
      data => {
        console.log('first_name: ' + form.value.fName)
        console.log('last_name: ' + form.value.lName)
        console.log('email: ' + form.value.email)
        console.log('password: ' + form.value.password)
        this.dismissRegister();
        this.navCtrl.navigateRoot('/dashboard');
      },
      error => {
        console.log(error);
        console.log("Unsuccessfully registered! Repeat again!");
      },
      () => {
        
      }
    );
  }
}